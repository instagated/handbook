---
title: TTSJobs
tags:
  - TTS Jobs
  - tts jobs
  - TTSJobs
  - ttsjobs
  - details
  - Details
  - jobs
  - Jobs
  - job
  - Promotion
  - Promotions
  - promotion
  - promotions
  - staffing plan
  - Staffing Plan
  - staffing plans
redirect_from:
  - /ttsjobs/
---

This page serves as a central listing for:

- TTS Internal competitions (e.g. directors, supervisors) and developmental opportunities (e.g. details, leads) within TTS
- Hiring actions using the Excepted Service version of the Merit Promotion Process
- Links to USAJobs Announcements for hiring actions using the Competitive Service (Career) Merit Promotion Process
- Links to Excepted Service job announcements posted on the TTS Join site
- Links to external technology career opportunities with federal, state, and local government agencies

If you’d like to be notified when new opportunities are listed on this page, please join the [\#tts-jobs](https://gsa-tts.slack.com/messages/tts-jobs/) (for TTS opportunities) and [\#wg-govcareer](https://gsa-tts.slack.com/messages/wg-govcareer) (for opportunities with other federal agencies) Slack channels.

If you know an awesome person for a TTS role or just know a great person that you'd like to recommend in general, please check out the [referring a person]({{site.baseurl}}/office-of-operations/talent/#referring-a-person) process.

## Announcements

### Open

- [Centers of Excellence - Modernization Lead (IT Specialist)](https://join.tts.gsa.gov/join/coe-modernization-lead/) - open Friday, October 29th until Monday, November 15th
- [Technology Transformation Services Engineer (IT Specialist)](https://join.tts.gsa.gov/join/tts-engineer/) - open Friday, October 29th until Monday, November 15th
- [Technology Transformation Services Product Manager/Project Manager (IT Specialist)](https://join.tts.gsa.gov/join/tts-product-project-manager/) - open Friday, October 29th until Monday, November 15th
- [U.S. Digital Corps Fellows](https://digitalcorps.gsa.gov/apply/) - open Monday, November 8th until 11:59pm ET on November 15th or at 11:59pm ET on the day that 300 applications in that track are received, whichever comes first

### Internal only opportunities

- [GSA WIDE: American Rescue Plan (ARP) Project Coordinator Detail](https://docs.google.com/document/d/1NUXetv6Z_pmlS_5WKVP1-YPTaK0IfLod08bBPorZnIw/edit#) - open Wednesday, October 27th until Friday, November 19th
- [TTS WIDE: TTS Leadership DEI&A Representative Detail](https://docs.google.com/document/d/1tJQSywrjZRfjgDVOf6cCH_MqVVjFIf-okym9Yk3_asU/edit#) - open Monday, November 1st until Friday, November 12th
- [TTS WIDE: Various positions 10x](https://docs.google.com/document/d/1Qj35ard4yww_86Ikw23FhIUgEGyiECDb074ZrFj3uDU/edit#) - open Monday, November 8 until Tuesday November 16th

### Open to all federal employees

We will share open positions as they are available.

## Opportunities with partners

**United States Digital Service (USDS)**

For any TTS staff interested in learning what it’s like to transition from TTS to USDS or just what it's like to work at USDS in general, a few of the TTS alums have offered to make themselves available for some informational discussions. If you’re interested in learning more, simply fill out this [USDS Informational Meeting Request form](https://docs.google.com/forms/d/e/1FAIpQLSfzbkhF6ahHv8-mu3BOpl6l7qg_kVyHuGUpDMcA-cPW60BfoQ/viewform?usp=sf_link) and someone from USDS will reach out to get something set up.

Be sure to stay informed of current job postings relevant to technology transformation in TTS and with other agencies by joining the [\#tts-jobs](https://gsa-tts.slack.com/messages/tts-jobs/) Slack channel. If you want to be notified about opportunities with other federal agencies please join the [\#wg-govcareer](https://gsa-tts.slack.com/messages/wg-govcareer) Slack channel. We encourage TTS staff to post external federal career opportunities on the [\#wg-govcareer](https://gsa-tts.slack.com/messages/wg-govcareer) Slack channel to help others be aware of permanent, career opportunities. TTS staff can post opportunities advertised by our federal partners to help them attract the engineering, product management, and leadership talent they may require to further facilitate their technical transformation.

**Various Federal Agencies**

---

### Still have questions?

**Have questions about an announcement listed?** Please reach out to the Hiring PoC listed on the announcement

**General questions** Please reach out to TTS Talent via [\#tts-jobs](https://gsa-tts.slack.com/messages/tts-jobs/) or [email](mailto:tts-talentteam@gsa.gov) for information regarding hiring.
